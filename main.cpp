#include <iostream>
#include <fstream>
#include <string>
#include <cmath>
#include <vector>
#include <chrono>
#include <pagmo/algorithm.hpp>
#include <pagmo/problem.hpp>
#include <pagmo/types.hpp>
#include <pagmo/population.hpp>
#include <pagmo/algorithms/nlopt.hpp>
#include <pagmo/utils/gradients_and_hessians.hpp>
#include <pagmo/island.hpp>
#include <pagmo/archipelago.hpp>
#include <pagmo/island.hpp>
#include <pagmo/archipelago.hpp>
#include <pagmo/topology.hpp>
#include <pagmo/topologies/ring.hpp>
#include <pagmo/topologies/fully_connected.hpp>

#include <pagmo/problems/unconstrain.hpp>
#include <pagmo/algorithms/nsga2.hpp>
#include <pagmo/utils/multi_objective.hpp>

#include "merge_population.h"

using namespace pagmo;
double P{6000};
double C{3.6587*pow(10,-4)};
double L{12.0};
double E{30*pow(10,6)};
double G{12*pow(10,6)};
class my_pb {
public:
   my_pb(){bounds = {{0.125,0.1,0.1,0.125},{5.,10.,10.,5.}}; }
    vector_double fitness(const vector_double &x) const;
    std::pair<vector_double,vector_double> get_bounds() const
    {
        return {{0.125,0.1,0.1,0.125},{5.,10.,10.,5.}};
    }
    void set_bounds(std::pair<vector_double,vector_double> &bds)
    {
        bounds = bds;
    }
    vector_double::size_type get_nobj() const { return 2; }
    vector_double::size_type get_nec() const { return 0; }
    vector_double::size_type get_nic() const {return 4; }

    vector_double gradient(const vector_double &x) const {
        return estimate_gradient_h(
            [=](const vector_double &_x) { return fitness(_x); }, x, 1e-8);
      }

    double tau_1(const vector_double &x)const {  return 1/(sqrt(2)*x[0]*x[1]);}
    double R(const vector_double &x)const {return sqrt( pow(x[1],2) + pow(x[0]+x[2],2) );}
    double tau_2(const vector_double &x)const {
        return (L+x[1]/2)*R(x)/(sqrt(2)*x[0]*x[2]*(pow(x[1],2)/3+pow(x[0]+x[2],2)));
    }
    double tau(const vector_double &x)const {return P*sqrt(pow(tau_1(x),2) +pow(tau_2(x),2) + 2/R(x)*(tau_1(x)*tau_2(x)*x[1]));}
    double buckl(const vector_double &x)const {return 4.013*E*x[2]*pow(x[3],3)/(6*pow(L,2))*(1-x[2]/(2*L)*sqrt(E/(4*G)) );}
private:
    std::pair<vector_double,vector_double> bounds;
};

vector_double my_pb::fitness(const vector_double &x) const{
    return {
        1.10471*pow(x[0],2)*x[1] + 0.04811*x[2]*x[3]*(14+x[1]), //F1
        P*C/(x[3]*pow(x[2],3)),                            //F2
        //ineq
        x[0]-x[3],
        tau(x)-13600,
        P*6*L/(x[3]*pow(x[2],2))-30*pow(10,3),
        6000-buckl(x)
    };
}

int main(){
    // start timer
    auto start = std::chrono::high_resolution_clock::now();
    std::cout << "Starting timer...\n";

    problem p{my_pb{ }};
    vector_double w{{50,50,50,50}};
    auto up = unconstrain(p, "weighted", w);
    int npop = 8;
    int ngen = 1000;
    population pop(up,npop);
    algorithm algo{nsga2(ngen)};
    algo.set_verbosity(0);
    archipelago archi = archipelago(ring());
    int narch{36};

    for (int i = 0; i<narch; ++i){
        population pop(up,npop);
        island isl = island(algo, pop);
        archi.push_back(isl);
    }

    archi.evolve(10);
    archi.wait_check();
    population m_pop = merge_population(up, archi);


    //stop timer
    std::cout << "...stopping timer.\n Total duration:";
    auto stop = std::chrono::high_resolution_clock::now();
    auto duration = std::chrono::duration_cast<std::chrono::seconds>(stop - start);
    std::cout << duration.count() << " seconds (MERGED)" << std::endl;

    std::ofstream outdata;

    std::string fname = "arch_cmp_fc/pareto_p_MERGED_archi_";
    fname.append(std::to_string(npop));
    fname.append("_");
    fname.append(std::to_string(ngen));
    fname.append("_nisl_");
    fname.append(std::to_string(narch));
    fname.append(".txt");
    outdata.open(fname);
    if (!outdata){std::cerr<<"File could not be opened"; exit(1);}
    outdata << "Duration: " << duration.count() << std::endl;
    population evolved(up,0);
    std::vector front =  pagmo::non_dominated_front_2d(m_pop.get_f()); // restituisce un vettore con gli indici del fronte

    for (int i = 0; i<front.size(); ++i){
        outdata << m_pop.get_f()[front[i]][0] << " , " << m_pop.get_f()[front[i]][1] << std::endl;
    }
    outdata.close();

}
