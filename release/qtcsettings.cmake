# This file is managed by Qt Creator, do not edit!

set("CMAKE_BUILD_TYPE" "Release" CACHE "STRING" "" FORCE)
set("CMAKE_PROJECT_INCLUDE_BEFORE" "C:/Qt/Tools/QtCreator/share/qtcreator/package-manager/auto-setup.cmake" CACHE "PATH" "" FORCE)
set("QT_QMAKE_EXECUTABLE" "C:/Qt/6.2.3/msvc2019_64/bin/qmake.exe" CACHE "STRING" "" FORCE)
set("CMAKE_PREFIX_PATH" "C:/Qt/6.2.3/msvc2019_64" CACHE "STRING" "" FORCE)
set("CMAKE_C_COMPILER" "C:/Program Files (x86)/Microsoft Visual Studio/2019/Community/VC/Tools/MSVC/14.29.30133/bin/HostX64/x64/cl.exe" CACHE "STRING" "" FORCE)
set("CMAKE_CXX_COMPILER" "C:/Program Files (x86)/Microsoft Visual Studio/2019/Community/VC/Tools/MSVC/14.29.30133/bin/HostX64/x64/cl.exe" CACHE "STRING" "" FORCE)
set("Eigen3_DIR" "C:\Lib\eigen\install\release\share\eigen3\cmake" CACHE "UNINITIALIZED" "" FORCE)
set("NLopt_DIR" "C:\Lib\nlopt\install\release\lib\cmake\nlopt" CACHE "UNINITIALIZED" "" FORCE)
set("Pagmo_DIR" "C:\Lib\pagmo\install\release\lib\cmake\pagmo" CACHE "UNINITIALIZED" "" FORCE)