#ifndef MERGE_POPULATION_H
#define MERGE_POPULATION_H


#include <iostream>
#include <vector>
#include <pagmo/algorithm.hpp>
#include <pagmo/problem.hpp>
#include <pagmo/types.hpp>
#include <pagmo/population.hpp>
#include <pagmo/archipelago.hpp>
#include <pagmo/problems/unconstrain.hpp>
using namespace pagmo;
population merge_population(problem p, archipelago a);
population merge_population(pagmo::unconstrain p, archipelago a);
#endif // MERGE_POPULATION_H
