#include "merge_population.h"

population merge_population(problem p, archipelago a){
    int sz = a.size();
    population merged(p, 0);
    for (int j = 0; j<sz; ++j){
        population temp = a[j].get_population();
        std::vector<vector_double> v = temp.get_x();
        for (int k = 0; k<v.size(); ++k){merged.push_back(v[k]);}
    }
    return merged;
}

population merge_population(pagmo::unconstrain p, archipelago a){
    int sz = a.size();
    population merged(p, 0);
    for (int j = 0; j<sz; ++j){
        population temp = a[j].get_population();
        std::vector<vector_double> v = temp.get_x();
        for (int k = 0; k<v.size(); ++k){merged.push_back(v[k]);}
    }
    return merged;
}
